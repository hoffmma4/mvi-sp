# mvi-sp


## Assignment
The Assignment is based on [this](https://www.kaggle.com/competitions/gan-getting-started) kaggle competition. The task is about image to image translation using CycleGan model. 

## Info
The CycleGan jupyter notebook contains all the code and models. For it to work, the data.zip package has to be unziped, since it contains all the training data. I have tried to write the code as clean as possible to make it more readable. 

There are also images already generated with the trained network in the other zip package. Unfortunately, the trained model couldn't be included, since the size is over 1GB. 

